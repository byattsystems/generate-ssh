<?php include "inc/randompassword.inc.php";?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title>SSH Keys Generator</title>
    <link rel="stylesheet" href="css/cerulean/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css" >
  </head>
  <body>
    <div class="container" role="main">

      <?php include "mainmenu.php";?>

      <div class="row">
        <div class="col-md-12">
          <div class="jumbotron">
            <h1>Generate Password(s)</h1>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-4">
          <div class="well">
            <?php for($i=0;$i<=10;$i++) {
              $password = randompassword(32);
              echo "<div>$password</div>";
            } ?>
          </div>
        </div>

        <div class="col-md-4">
          <div class="well">
            <?php for($i=0;$i<=10;$i++) {
              $password = randompassword(32);
              echo "<div>$password</div>";
            } ?>
          </div>
        </div>

        <div class="col-md-4">
          <div class="well">
            <?php for($i=0;$i<=10;$i++) {
              $password = randompassword(32);
              echo "<div>$password</div>";
            } ?>
          </div>
        </div>
      </div>

    </div>
    <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/docs.min.js"></script>
  </body>
</html>