<?php
function increment($numberfile, $readonly = false) {
  if(file_exists($numberfile)) {
    $number = file_get_contents($numberfile);
  } else {
    $number = 1;
  }
  if($readonly === false) {
    $number++;
    file_put_contents($numberfile, $number);
  }
  return $number;
}
?>