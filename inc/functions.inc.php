<?php
function vd($o, $t = "") {
    echo "<pre>";
    echo ($t<>""?"$t: ":"");
    var_dump($o);
    echo "</pre>";
}

function vdd($o, $t = ""){
    vd($o, $t);
    die;
}
?>