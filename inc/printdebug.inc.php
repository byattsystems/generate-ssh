<?php
$debugenabled = false;

function printdebug($data, $text = "") {
  global $debugenabled;
  if($debugenabled) {
    echo "<pre id=\"printdebug\">";
    if($text != "") {
      echo "$text: ";
    }
    print_r($data);
    echo "</pre>";
  }
}
?>